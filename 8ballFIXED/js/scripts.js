//Global variables
//create variables for all DOM objects that will be part of the
//application interactions
var code
var random
var msg
$(function() {
    reset8Ball();
    $('form').submit(function(e) {
        e.preventDefault();
        getFortune();
    });
});

//Helper functions
var getRandomIndex = function(max) {
    code = $('#question').val();
};


var showAnswer = function(msg) {
    getRandomIndex();
    $('#answer').html(msg).delay('700').fadeIn('fast');
    $('img').delay('700').fadeOut('fast');
};


var getFortune = function() {
    $('#magic8ball').effect("shake", {times:2,direction:"up"}, 100);
    $.ajax({
        url:'php/fortune.php',

        type:'post',

        data: {"code":code},

        success: function(response) {
            showAnswer(response);
        },

        error: function(xhr) {
            showAnswer('TYPE A THING PLZ');
        },
    });
};

var reset8Ball = function() {
    $('#magic8ball').click(function(){
        $('#magic8ball').effect("shake", {times:1,direction:"up"}, 100);
        $('#answer').html('').fadeOut('fast');
        $('img').fadeIn('fast');
    });
};
